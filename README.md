Gitlab Pipelines Task
====


El objetivo de este ejercicio es investigar sobre Gitlab Pipelines y crear un pipeline que en escencia servira para entender con más facilidad el pipeline utilizado en Omedym.

Este repositorio cuenta con dos proyectos de frontend: `frontend-1` y `frontend-2` y un proyecto de backend `backend-1`.

Las aplicaciones de frontend son dos proyectos creados cone `create-react-app` con todos los parametros por defecto.

La aplicacion de backend fue creada con todos los parametros por defecto utilizando Serverless version 2.


## Que se busca con este pipeline
El pipeline debe tener las siguientes características:
- Deben haber diferentes [stages](https://docs.gitlab.com/ee/ci/pipelines/#cicd-pipelines): build-frontend, build-backend y deploy
- Para el stage de build-frontend
  - Para el frontend: se debe construir (investigar como construir una aplicacion React `hint: revisa el package.json`). El resultado del build en la carpeta `dist` guardarlo en un .zip y persistirlo como [artifact](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html)
- Para el stage de build-backend
  - Para el backend: se debe crear un `package` utilizando el comando de Serverless. El resultado del build en la carpeta `.serverless` tiene un archivo .zip, persistir ese .zip como un [artifact](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html).
- Para el build de todos los proyectos, crear un Makefile que haga el build ya sea de las aplicaciones React o Serverless. Y este Makefile debe ser llamado desde la definicion YAML del pipeline, e.g. `make build-app`.
- Para el stage del deploy:
  - puedes hacer unos scripts en Terraform que no usen un cloud provider, solamnente es para simular un deploy, por ejemplo crear [local files](https://registry.terraform.io/providers/hashicorp/local/latest/docs/resources/file) en base a un [template](https://registry.terraform.io/providers/hashicorp/template/latest/docs/data-sources/file).
  - Similar al build crear un Makefile donde se ejecute un plan y un apply de Terraform.

## Mejorando el pipeline
- Ya una vez se tenga definido el pipeline para hacer build y deploy, se debe segmentar y hacer que Gitlab pipelines solo haga build cuando un proyecto se modifique, es decir, si un desarrollador modifica `frontend-1` que el pipeline solo ejecute el job de `frontend-1` y no de los demas proyectos. Para eso ver sobre los [Rules](https://registry.terraform.io/providers/hashicorp/template/latest/docs/data-sources/file) en Gitlab
